import { useContext, useEffect, useState } from "react";
import PostsContext from "../components/posts-context/posts-context.js";

const useFetchGet = () => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const {postsService} = useContext(PostsContext);

    useEffect(() => {
        const fetchData = () => {

            setLoading(true);
            setError(null);

            try {

                postsService.get()
                .then(res => setData(res));
                
            } catch(ex) {
                setError(ex);
            } finally {
                setLoading(false);
            }
        };

        fetchData();

    }, []);


    return [{data, loading, error}];
}

export default useFetchGet;