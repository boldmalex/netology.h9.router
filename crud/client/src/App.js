import react from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import PostAdd from "./components/post-add/post-add.js";
import PostShow from "./components/post-show/post-show.js";
import PostsPage from "./components/posts-page/posts-page.js";
import PostsContextProvider from "./components/posts-context-provider/posts-context-provider.js"
import "./index.css";



function App() {
  return (
    <PostsContextProvider>
      <Router>
        <div className="page">
          <Routes>
            <Route path="/" element={<PostsPage/>}/>
            <Route path="home" element={<PostsPage/>}/>
            <Route path="new" element={<PostAdd/>}/>
            <Route path="posts/:id" element={<PostShow/>}/>
          </Routes>
        </div>
      </Router>
    </PostsContextProvider>
  );
}

export default App;
