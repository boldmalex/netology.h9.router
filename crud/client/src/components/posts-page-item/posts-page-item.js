import react from "react";
import {useNavigate} from "react-router-dom";


const PostsPageItem = (props) => {
    const {id, content, created} = props;
    const navigate = useNavigate();

    const handleClick = () => {
        navigate(`posts/${id}`, {state: props});
    }

    const showTime = () =>{

        var date = new Date(created);
        return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
    }

    return (
        <div className="post-card" onClick={handleClick}>
            <article>
                <h4>Создан {showTime()}</h4>
                <p>
                    {content}
                </p>
            </article>
        </div>
    );
}

export default PostsPageItem;