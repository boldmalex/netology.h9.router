import react, {useState, useContext, useEffect} from "react";
import { useNavigate, useParams } from "react-router-dom";
import PostsContext from "../posts-context/posts-context.js";


const PostShow = () => {
    
    const {postsService} = useContext(PostsContext);
    const [isEditting, setEditing] = useState(false);
    const {id : pathId} = useParams();
    const [post, setPost] = useState(null);
    const navigate = useNavigate();

    
    useEffect(() => {
        postsService.getById(pathId)
                    .then(post => setPost(post))
    }, []);
        

    // Изменение текста в редакторе поста
    const onHandleEditContent = ({target}) => {
        const name = target.name;
        const value = target.value;
        setPost(last => ({...last, [name]:value}));
    }

    // Кнопка изменить в редакторе поста
    const onHandleEdit = (evt) => {
        postsService.put(post.id, post.content);
        setEditing(!isEditting);
    }

    // Кнопка отменить в редакторе поста
    const cancelEdit = (evt) => {
        evt.preventDefault();
        setEditing(!isEditting);
    }


    // Кнопка отменить в показе поста
    const cancelShowPost = (evt) => {
        evt.preventDefault();
        navigate("/", {replace:true});
    }
    
    // Кнопка вызова редатора поста
    const editPost = (evt) => {
        setEditing(!isEditting);
    }

    // Кнопка удаления поста
    const removePost = (evt) => {
        evt.preventDefault();
        postsService.remove(post.id)
        .then(navigate("/", {replace:true}));
    }

    // Отобразить дату поста
    const showTime = () => {

        var date = new Date(post.created);
        return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
    }

    // Показ редактора
    const showEditor = () => {
        return (
            <div className="post-card">
                <div className="x-cancel" onClick={cancelEdit}>
                    X
                </div>
                <div>
                    <label htmlFor="content">Текст: </label>
                    <input className="post-text"
                            type="text"
                            id="content"
                            name="content"
                            value={post.content}
                            onChange={onHandleEditContent}
                    />
                </div>
                <div className="post-button">
                    <button onClick={onHandleEdit}>Изменить</button>
                </div>
            </div>
        );
    }


    // ------ UI --------
    // ------------------

    // Если в режиме редактирования
    if (post && isEditting) return (
        <>
            {showEditor()}
        </>
    );

    // Если в режиме показа
    if (post)
    {
        return (
            <div className="post-card">
                <div className="x-cancel" onClick={cancelShowPost}>
                    X
                </div>
                <article>
                    <h4>Создан: {showTime()}</h4>
                    <p>
                        {post.content}
                    </p>
                </article>
                <div className="post-button">
                        <button onClick={editPost}>Изменить</button>
                        <button onClick={removePost}>Удалить</button>
                </div>
            </div>
        );
    }
    else return null;
}


export default PostShow;