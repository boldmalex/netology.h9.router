import react from "react";
import PostsContext from "../posts-context/posts-context";
import { PostsService } from "../posts-service/posts-service";


const PostsContextProvider = (props) => {
    const postsService = new PostsService();

    return (
        <PostsContext.Provider value={{postsService}}>
            {props.children}
        </PostsContext.Provider>
    )
}

export default PostsContextProvider;