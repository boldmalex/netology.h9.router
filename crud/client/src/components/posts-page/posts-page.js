import react from "react";
import { NavLink } from "react-router-dom";
import useFetchGet from "../../hooks/use-fetch-get.js";
import PostsPageItem from "../posts-page-item/posts-page-item";



const PostsPage = () => {
    const [{data, loading, error}] = useFetchGet();

    const showPosts = (data) => {
        return (
            data.map(item => <PostsPageItem key={item.id} {...item}/>)
        );
    }

    return (
        <>
            {loading&&<p>Loading...</p>}
            {error&&<p>{error}</p>}
            {data && <div>
                        <div>
                            <NavLink to="new">Добавить</NavLink>
                        </div>
                        { showPosts(data) }
                     </div>
            }
        </>
    );
}

export default PostsPage;