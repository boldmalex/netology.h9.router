
const url = "http://localhost:7777/posts";

class PostsService {
    
    async get() {
        const response = await fetch(url);
        return await response.json();
    }

    async getById(id) {
        const response = await fetch(`${url}/${id}`);
        return await response.json();
    }

    async post(cnt) {
        const content = { id:0, content:cnt };
        
        return await fetch(url, {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type":"application/json"
            }
        }).then(res => {
            if (!res.ok)
                throw "post error";
        });
    }

    async put(id, cnt) {
        const content = { id:id, content:cnt };
        return await fetch(url, {
            method: "POST",
            body: JSON.stringify(content),
            headers: {
                "Content-Type":"application/json"
            }
        }).then(res => {
            if (!res.ok)
                throw "put error";
        });
    }


    async remove(id){
        return await fetch(`${url}/${id}`, {
            method: "DELETE",
        }).then(res => {
            if (!res.ok)
                throw "put error";
        });
    }
    
}

export {PostsService};