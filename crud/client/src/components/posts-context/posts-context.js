import react from "react";

const PostsContext = react.createContext();

export default PostsContext;