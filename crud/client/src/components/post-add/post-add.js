import react, {useContext, useState} from "react";
import { useNavigate } from "react-router-dom";
import PostsContext from "../posts-context/posts-context.js";



const PostAdd = (props) => {

    const [content, setContent] = useState("");
    const {postsService} = useContext(PostsContext);
    const navigate = useNavigate();
    

    const addPost = (evt) => {
        evt.preventDefault();
        postsService.post(content)
        .then(navigate("/", {replace:true}));
    }

    const onChangeItem = ({target}) =>{
        const value = target.value;
        setContent(value);
    }  

    const cancel = (evt) => {
        evt.preventDefault();
        navigate("/", {replace:true});
    }


    return (
        <form onSubmit={addPost}>
            <div className="post-card">
                <div className="x-cancel" onClick={cancel}>
                    X
                </div>
                <div>
                    <label htmlFor="content">Текст: </label>
                    <input className="post-text"
                            type="text"
                            id="content"
                            name="content"
                            value={content}
                            onChange={onChangeItem}/>
                </div>
                <div className="post-button">
                    <button type="submit">Опубликовать</button>
                </div>
            </div>
        </form> 
    );
}

export default PostAdd;