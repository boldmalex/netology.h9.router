import react from "react";
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import DriftPage from "./components/drift-page/drift-page.js";
import ForzaPage from "./components/forza-page/forza-page.js";
import HomePage from "./components/home-page/home-page.js";
import Menu from "./components/menu/menu.js";
import TimeAttackPage from "./components/time-attack-page/time-attack-page.js";
import "./index.css";


function App() {
  return (
      <Router>
        <div>
          <Menu/>
          <div className="page">
            <Routes>
              <Route path="/" element={<HomePage/>}/>
              <Route path="home" element={<HomePage/>}/>
              <Route path="drift" element={<DriftPage/>}/>
              <Route path="forza" element={<ForzaPage/>}/>
              <Route path="time-attack" element={<TimeAttackPage/>}/>
            </Routes>
          </div>
        </div>
      </Router>
  );
}

export default App;