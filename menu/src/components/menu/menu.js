import react from "react";
import { NavLink } from "react-router-dom";
import "./menu.css";


const Menu = () => {

    return (
        
            <nav className="menu">
                <NavLink to="home" className="menu__item">ГЛАВНАЯ</NavLink>
                <NavLink to="drift" className="menu__item">ДРИФТ-ТАКСИ</NavLink>
                <NavLink to="forza" className="menu__item" >TIME-ATTACK</NavLink>
                <NavLink to="time-attack" className="menu__item" >FORZA-KARTING</NavLink>
            </nav>
        
    );
} 

export default Menu;